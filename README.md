# Group meetings #

## Unfinished business

* Sebastian: Status Poster im Gang
* Visualize uncertain things with CART
* Wolfgang - group goals continued
* Julian - Publishing software on our website - license and more
* Group: Industry-relevant Bachelor Topics

## Schedule


| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|28.06.2017 |  |  | No groupmeeting |
|05.07.2017 |  | Aline: data collection and presentation, ideas of topics | |
|12.07.2017 |  | Minimilestones, Claudius (Sebastian) and Delia (Jannik)  | Wolfgang is late |
|19.07.2017 | Hugues, Christine (Sebastian)| Final presentation | Micha is late |
|26.07.2017 | Micha | Automatic miracles | |
|02.08.2017 | Abelardo | Particle Swarm | |
|23.08.2017 |  | Final Presentation Eric (Jannik) | |
|06.09.2017 |  | Minimilestone Yoga (Jannik)  | |

## Possible future topics
* Introduction to:
    * MOO (Felix)
    * Bayesian Thinking (Micha)
* Supervision Guidelines
* Workshop on extrem programming PCE (Sergey, Anneli)
* Reihum-Vorträge, z.B.
    * Entropy-Article, Wolfgang's Master Thesis (Anneli, Wolfgang) Soon!
* Sergey: new stuff like Bitcoins


### History
#### 2017
| Date | Who | Topic | Remark |
| :--- | :-- | :---- | ------ |
|01.03.2017 | Jannik | Grammarly | Mini-Milestone Natalie (Jannik) |
|08.03.2017 |  | AGU Poster post-mortem | Mini-Milestone Sebastian (Jannik) |
|15.03.2017 | Felix | Special Issue Paper| |
|22.03.2017 | Marvin | What is model complexity ? | |
|29.03.2017 | Ana | Present her work | |
|12.04.2017 | Reynold | Presentation of Evan (20min + 10min questions), EGU Poster preview (5min + 15min discussion)| |
|19.04.2017 | Reynold | Modflow Magic | 11:30 - 12:15 |
|26.04.2017 |  |  | EGU (no meeting) |
|03.05.2017 | Wolfgang | Group Goals | |
|10.05.2017 | Jannik | Optimization with GAMS | Scheduling |
|17.05.2017 |  |  | No groupmeeting |
|24.05.2017 | Anneli | Risk paper | |
|31.05.2017 |  |  | No groupmeeting |
|07.06.2017 |  |  | No groupmeeting |
|14.06.2017 | Group, Wolfgang, Marvin, Abelardo | Bachelor Topics, SFB Helfer, TDW Helfer, Publication-List Check |  |
|21.06.2017 | Jannik, Marvin, Reynold, Sergey | Reviewlets, Scheduling | Bam items: the right use of our group calenders, ENWAT doctoral seminar: who wants to present? Abstracts AGU |


#### 2016 

* 13.01. Set up Schedule
* 20.01. Programmdemo (Felix), Make a Schedule for next weeks
* 27.01. entfällt -> Tübingen
* 01.02. (Montag) Probevortrag Anneli 9:30
* 03.02. Entfällt -> Alle in Tübingen
* 10.02. Julian - Hypothesis Testing + Credibility Interval vs. Confidence Interval
* 17.02. 30s: Micha + Dissertation Title,
* 24.02. 30s: Abelardo and Julian
* 02.03. Felix - project title, group picture
* 09.03. Presentation (Reynold)
* 16.03. 30s: Abelardo
* 23.03. (Wolfgang not here) 30s: Julian
* 30.03. (Wolfgang not here) Introduction to PCE: Sergey
* 06.04. 
* 13.04. Julian and everybody: Backup party
* 20.04. - no meeting, EGU -
* 27.04. Anneli: Project proposal
* 04.05. Tag der Wissenschaft, IWS-Vollversammlung, Julian: Paper title
* 11.05. Felix: Paper title
* 18.05. Abelardo: transient catchments
* 25.05. Julian Summary of Paper (if applicable) + Error modeling 
* 01.06. Bucket-Model Session (Reynold, Marvin)
* 08.06. Jannik, Extension planing
* 15.06. Tag der Wissenschaft
* 22.06. Wolfgang - Report von Schulung
* 29.06. Micha - Surrogate modeling
* 06.07. Nelson
* 13.07. Marvin BME
* 20.07. Mini-Milestone Raphael
* 27.07. Mini-Milestone
* 03.08. Melanie
* 10.08. 
* 17.08. Anneli - Paper-Titel-Findung
* 24.08.
* 31.08. Ideas for Anjas present, Software für die Lehre aus Qualitätssicherungsmitteln
* 14.09. Sebastian - Milestone-Testvortrag   [Vor-Ort-Begutachtung Tübingen]
* 21.09. Wolfgang und alle - "Risiko"
* 28.09. Wolfgang - Group Goals (cont'd)
* 05.10. Anneli - Choosing between thermodynamic models with Bayesian model selection
* 12.10. no meeting (rooms are taken)
* 19.10. Felix - Studentenarbeits-Bewertungsschema
* 26.10. no meeting
* 09.11. Wolfgang - Ty Ferre group discussion and maybe group goals (continued)
* 16.11. Betsaida Master's thesis presentation 

#### 2015

* 28.10. 30s: Sebastian, Anneli / Die Große Podiums-Kopula-Diskussion - Andi / New SMH Structure
* 04.11. 30s: Jannik, Sebastian / Informal Talk: MCMC - Wolfgang
* 11.11. 30s: Jannik, Marvin, Reynold / Stochastic Optimization in Power Systems - Jannik
* 18.11. 30s: Sergey, Wolfgang / Code of Conduct
* 25.11. (Status Seminar SimTech)
* 02.12. 30s: Felix / Mitarbeitergespräche Guidelines
* 09.12. 30s: Micha / Micha: Selbstplagiat
* 16.12. (AGU) - no meeting

#### 2014
* 27.10. Hypothesis exercise, Title exercise
* 03.11. Scientific Writing II
* 10.11. Sebastian
* 17.11. Anneli
* 24.11. entfällt
* 01.12. Codearchivierung (Julian und Micha)
* 08.12. Georgenhof / Wie rechnet man auf dem Cluster? / Umzug
* 15.12. entfällt - AGU
* 22.12. entfällt - keiner da
* 12.01. Präsentationsvorlagen - Micha
* 19.01. Logo - Julian / Layout Folien - Micha / Austausch: Softskillkurse - alle
* 26.01. How to present your work - Sebastian + Sergey
* 02.02. Versionskontrolle - Andi + Julian / Code teilen (Stand der Dinge)
* 09.02. entfällt - Wolfgang in Aachen
* 16.02. Was verspreche ich mir von einer Betreuungsbeziehung? - Felix fragt Anja oder macht es.
* 23.02. Ultraglattes Paper - Sergey
* 16.03. Überblick Bitchjobs, Journalwatch, Betreuungsverhältnis
* 23.03. Regeln für Klausuraufgaben, Laufendeanträge
* 30.03.